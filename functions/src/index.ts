import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as path from 'path'
// tslint:disable-next-line: no-implicit-dependencies
import { GetSignedUrlConfig } from '@google-cloud/storage'

admin.initializeApp()

export const generateURLs = functions.storage
    .object()
    .onFinalize(async object => {
        const db = admin.firestore()

        const filePath = object.name
        const fileBucket = object.bucket
        console.log('default bucket', fileBucket)
        const fileName = path.basename(filePath ? filePath : '')

        const bucket = admin.storage().bucket(fileBucket)

        const file = bucket.file(filePath ? filePath : '')

        try {
            const config: GetSignedUrlConfig = {
                action: 'read',
                expires: '03-01-2500',
            }

            const [url] = await Promise.all([file.getSignedUrl(config)])

            const payload = {
                fileName: fileName,
                filePath: filePath,
                url: url,
            }

            await db.collection('images').add(payload)
        } catch (error) {
            console.log(error)
        }
    })

export const deleteImg = functions.storage.object().onDelete(async object => {
    const db = admin.firestore()

    const filePath = object.name
    const fileName = path.basename(filePath ? filePath : '')

    try {
        const query = await db
            .collection('images')
            .where('fileName', '==', fileName)
            .where('filePath', '==', filePath)
            .limit(1)
            .get()
        console.log(query)

        let id: string | null = null

        query.forEach(doc => {
            if (doc.exists) {
                id = doc.id
            }
        })

        if (id) {
            const resp = await db
                .collection('images')
                .doc(id)
                .delete()
            console.log(resp)
        } else {
            console.log(new Error('Image not found in Database'))
        }
    } catch (error) {
        console.log(error)
    }
})

export const reGenerateSignURLs = functions.pubsub
    .topic('re-generate-signed-urls')
    .onPublish(async message => {
        const batch = admin.firestore().batch()
        const collection = admin.firestore().collection('images')

        const images: any[] = []
        const imagesRef = await collection.get()
        imagesRef.forEach(doc => {
            const data = doc.data()
            data.id = doc.id
            images.push(data)
        })

        try {
            const config: GetSignedUrlConfig = {
                action: 'read',
                expires: '03-01-2500',
            }

            for (const iterator of images.entries()) {
                const value = iterator[1]
                const file = admin
                    .storage()
                    .bucket('pubpackr-pubg-backpack.appspot.com')
                    .file(value.filePath)
                const [url] = await file.getSignedUrl(config)
                batch.update(collection.doc(value.id), {
                    url: url,
                })
            }
            await batch.commit()
            console.log('Urls Successfull Re-Generated')
        } catch (error) {
            console.log(error)
        }
    })

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/app/Home/Home.vue'
import Manager from '@/app/Manager/Manager.vue'
import Backpacks from '@/app/Manager/categories/Backpacks.vue'
import Barrel from '@/app/Manager/categories/Barrel.vue'
import Bullets from '@/app/Manager/categories/Bullets.vue'
import Magazines from '@/app/Manager/categories/Magazines.vue'
import Medkits from '@/app/Manager/categories/Medkits.vue'
import Muzzle from '@/app/Manager/categories/Muzzle.vue'
import Scopes from '@/app/Manager/categories/Scopes.vue'
import Stocks from '@/app/Manager/categories/Stocks.vue'
import Vests from '@/app/Manager/categories/Vests.vue'
import Throwables from '@/app/Manager/categories/Throwables.vue'
import Boost from '@/app/Manager/categories/Boost.vue'
import User from '@/app/User/User.vue'
import GetStarted from '@/app/User/GetStarted.vue'
import GuestForm from '@/app/User/GuestForm.vue'
import FullPageAd from '@/app/Sales/Ads/FullPageAd.vue'

import { Auth } from '@/app/Auth/auth.service'

import store from '@/store/store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/app',
        name: 'app',
        component: Manager,
        children: [
            {
                path: 'backpacks',
                name: 'backpacks',
                component: Backpacks,
            },
            {
                path: 'barrel',
                name: 'barrel',
                component: Barrel,
            },
            {
                path: 'bullets',
                name: 'bullets',
                component: Bullets,
            },
            {
                path: 'magazines',
                name: 'magazines',
                component: Magazines,
            },
            {
                path: 'medkits',
                name: 'medkits',
                component: Medkits,
            },
            {
                path: 'muzzle',
                name: 'muzzle',
                component: Muzzle,
            },
            {
                path: 'scopes',
                name: 'scopes',
                component: Scopes,
            },
            {
                path: 'stocks',
                name: 'stocks',
                component: Stocks,
            },
            {
                path: 'vests',
                name: 'vests',
                component: Vests,
            },
            {
                path: 'throwables',
                name: 'throwables',
                component: Throwables,
            },
            {
                path: 'boost',
                name: 'boost',
                component: Boost,
            },
        ],
        beforeEnter: async (to: any, from: any, next: Function) => {
            if (Auth.currentUser === null) {
                await store.dispatch('User/getUser')
            }
            try {
                if (Auth.currentUser !== null) {
                    next()
                } else {
                    next('/get-started')
                }
            } catch (error) {
                console.log(error)
            }
        },
    },
    {
        path: '/account',
        name: 'account',
        component: User,
        beforeEnter: async (to: any, from: any, next: Function) => {
            if (Auth.currentUser === null) {
                await store.dispatch('User/getUser')
            }
            try {
                if (Auth.currentUser !== null) {
                    await store.dispatch('Selection/getOfflineSelection')
                    next()
                } else {
                    next('/get-started')
                }
            } catch (error) {
                console.log(error)
            }
        },
    },
    {
        path: '/get-started',
        name: 'login',
        component: GetStarted,
        beforeEnter: async (to: any, from: any, next: Function) => {
            if (Auth.currentUser === null) {
                await store.dispatch('User/getUser')
            }
            if (Auth.currentUser === null) {
                next()
            } else {
                next('/app')
            }
        },
    },
    {
        path: '/guest',
        name: 'guest',
        component: GuestForm,
    },
    {
        path: '/ad',
        name: 'full-page-ad',
        component: FullPageAd,
        beforeEnter: async (to: any, from: any, next: Function) => {
            if (Auth.currentUser === null) {
                await store.dispatch('User/getUser')
            }
            if (Auth.currentUser !== null) {
                next()
            } else {
                next('/get-started')
            }
        },
    },
]

const router = new VueRouter({
    routes,
})

export default router

export interface IAdState {
    ads?: IAd[]
    error: false
}

export interface IAd {
    name: string
    title: string
    url: string
    posterImgUrl: string
    popupImgUrl: string
    videoUrl: string
}

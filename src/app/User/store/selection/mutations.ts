import { MutationTree } from 'vuex'
import { ISelectionState, ISelection } from '../../types/selection/selection'

export const mutations: MutationTree<ISelectionState> = {
    addUserSelection(state: ISelectionState, payload: ISelection) {
        state.selection = payload
    },
}

import { Module } from 'vuex'
import { RootState } from '@/store/types'
import { ISelectionState } from '../../types/selection/selection'
import { mutations } from './mutations'
import { actions } from './actions'

export const state: ISelectionState = {
    selection: undefined,
    error: false,
}

const namespaced: boolean = true

export const Selection: Module<ISelectionState, RootState> = {
    namespaced,
    state,
    actions,
    mutations,
}

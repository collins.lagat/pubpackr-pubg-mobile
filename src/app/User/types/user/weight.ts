export interface IAppWeight {
    name: string
    number: number
    unitWeight: number
    weight: number
}

export interface IWeight {
    id?: number
    name: string
    number: number
    category: string
}
export interface INativeWeights {
    [key: string]: any
    barrel?: IAppWeight[]
    scopes?: IAppWeight[]
    stocks?: IAppWeight[]
    muzzle?: IAppWeight[]
    throwables?: IAppWeight[]
    bullets?: IAppWeight[]
    magazines?: IAppWeight[]
    medkits?: IAppWeight[]
    boost?: IAppWeight[]
}

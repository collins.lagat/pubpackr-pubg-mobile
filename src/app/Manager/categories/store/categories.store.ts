import { Module, MutationTree, ActionTree } from 'vuex'
import { RootState } from '@/store/types'
import { ICategoriesState, ICategories } from './types'

const categories: ICategories = {
    medkits: [
        {
            name: 'Bandages',
            weight: 2,
        },
        {
            name: 'First Aid Kit',
            weight: 10,
        },
        {
            name: 'Medkit',
            weight: 20,
        },
    ],
    boost: [
        {
            name: 'Energy Drink',
            weight: 4,
        },
        {
            name: 'Painkiller',
            weight: 10,
        },
        {
            name: 'Adrenaline Syringe',
            weight: 20,
        },
    ],
    bullets: [
        {
            name: '9mm',
            weight: 0.375,
        },
        {
            name: '0.45 ACP',
            weight: 0.4,
        },
        {
            name: '5.56mm',
            weight: 0.5,
        },
        {
            name: '7.62mm',
            weight: 0.7,
        },
        {
            name: '0.300 Magnum',
            weight: 1,
        },
        {
            name: '12 Gauge Shotgun Shell',
            weight: 1.25,
        },
        {
            name: 'Crossbow Bolt',
            weight: 2,
        },
        {
            name: 'Flare',
            weight: 0.5,
        },
    ],
    magazines: [
        {
            name: 'Extended Quickdraw Mag (Pistols, SMG)',
            weight: 17,
        },
        {
            name: 'Extended Mag (Pitols, SMG)',
            weight: 13,
        },
        {
            name: 'Quickdraw Mag (Pitols, SMG)',
            weight: 12,
        },
        {
            name: 'Extended Quickdraw Mag (AR, DMR)',
            weight: 20,
        },
        {
            name: 'Extended Mag (AR, DMR)',
            weight: 15,
        },
        {
            name: 'Quickdraw Mag (AR, DMR)',
            weight: 14,
        },
        {
            name: 'Extended Quickdraw Mag (Sniper, DMR)',
            weight: 23,
        },
        {
            name: 'Extended Mag (Sniper, DMR)',
            weight: 17,
        },
        {
            name: 'Quickdraw Mag (Sniper, DMR)',
            weight: 16,
        },
    ],
    throwables: [
        {
            name: 'Frag Grenade',
            weight: 16,
        },
        {
            name: 'Stun Grenade',
            weight: 12,
        },
        {
            name: 'Smoke Grenade',
            weight: 14,
        },
        {
            name: 'Molotov Coctail',
            weight: 18,
        },
        {
            name: 'Gas Can',
            weight: 20,
        },
    ],
    muzzle: [
        {
            name: 'Pistol/ SMG Suppressor',
            weight: 15,
        },
        {
            name: 'AR/ DMR Suppressor',
            weight: 20,
        },
        {
            name: 'Sniper/ DMR Suppressor',
            weight: 23,
        },
        {
            name: 'Pistol/ SMG Flash Hider',
            weight: 8,
        },
        {
            name: 'AR/ DMR Flash Hider',
            weight: 10,
        },
        {
            name: 'Sniper/ DMR Flash Hider',
            weight: 12,
        },
        {
            name: 'Pistol/ SMG Compensator',
            weight: 8,
        },
        {
            name: 'AR/ DMR Compensator',
            weight: 10,
        },
        {
            name: 'Sniper/ DMR Compensator',
            weight: 12,
        },
        {
            name: 'Choke',
            weight: 5,
        },
        {
            name: 'Duckbill',
            weight: 5,
        },
    ],
    stocks: [
        {
            name: 'Shotgun/ Sniper Bullet Loops',
            weight: 15,
        },
        {
            name: 'Sniper Cheek Pad',
            weight: 17,
        },
        {
            name: 'Micro Uzi Stock',
            weight: 10,
        },
        {
            name: 'M416/ Vector/ MP5K Tactical Stock',
            weight: 12,
        },
    ],
    scopes: [
        {
            name: 'Red Dot',
            weight: 10,
        },
        {
            name: 'Holographic',
            weight: 10,
        },
        {
            name: '2x',
            weight: 15,
        },
        {
            name: '3x',
            weight: 15,
        },
        {
            name: '4x',
            weight: 15,
        },
        {
            name: '6x',
            weight: 15,
        },
        {
            name: '8x',
            weight: 20,
        },
    ],
    barrel: [
        {
            name: 'Angled Foregrip',
            weight: 10,
        },
        {
            name: 'Vertical Foregrip',
            weight: 10,
        },
        {
            name: 'Half Grip',
            weight: 10,
        },
        {
            name: 'Light Grip',
            weight: 10,
        },
        {
            name: 'Thumb Grip',
            weight: 10,
        },
        {
            name: 'Laser Sight',
            weight: 10,
        },
        {
            name: 'Quiver for Crossbow',
            weight: 10,
        },
    ],
    backpacks: [
        {
            name: 'Level 1',
            capacity: 170,
        },
        {
            name: 'Level 2',
            capacity: 220,
        },
        {
            name: 'Level 3',
            capacity: 270,
        },
        {
            name: 'No Backpack',
            capacity: 0,
        },
    ],
    vests: [
        {
            name: 'No Vest',
            capacity: 0,
        },
        {
            name: 'Bulletproof Vest',
            capacity: 70,
        },
    ],
    character: [
        {
            name: 'body',
            capacity: 20,
        },
        {
            name: 'Utility Belt',
            capacity: 50,
        },
    ],
}

const namespaced: boolean = true

const state: ICategoriesState = {
    categories: categories,
    error: false,
}

const mutations: MutationTree<ICategoriesState> = {}
const actions: ActionTree<ICategoriesState, RootState> = {}

export const Categories: Module<ICategoriesState, RootState> = {
    namespaced,
    state,
    mutations,
    actions,
}

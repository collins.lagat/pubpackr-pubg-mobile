if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`)

    workbox.routing.registerRoute(
        /\.(?:js|css)$/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'static-resources',
        })
    );

    workbox.routing.registerRoute(
        /^https:\/\/fonts\.googleapis\.com/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'google-fonts-stylesheets',
        })
    )

    workbox.routing.registerRoute(
        /^https:\/\/storage\.googleapis\.com\/pubpackr-pubg-backpack\.appspot\.com/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'app-images',
        })
    )

    workbox.routing.registerRoute(
        /\.(?:png|gif|jpg|jpeg|webp|svg)$/,
        new workbox.strategies.CacheFirst({
            cacheName: 'static-images',
            plugins: [
                new workbox.expiration.Plugin({
                    maxEntries: 60,
                    maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
                }),
            ],
        })
    )

} else {
    console.log(`Boo! Workbox didn't load 😬`)
}

workbox.core.setCacheNameDetails({ prefix: "PubPackr-Pre-Cache" });

self.__precacheManifest = [].concat(self.__precacheManifest || []);

workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

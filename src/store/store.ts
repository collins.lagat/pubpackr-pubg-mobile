import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { Categories } from '../app/Manager/categories/store/categories.store'
import { URLS } from '../app/Manager/categories/store/urls.store'
import { User } from '@/app/User/store/user/user.store'
import { Ads } from '@/app/Sales/Ads/store/ads.store'
import { Selection } from '@/app/User/store/selection/selection.store'
import { RootState } from './types'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
    state: {
        version: '1.0.0',
    },
    modules: {
        Categories,
        URLS,
        User,
        Selection,
        Ads,
    },
}

export default new Vuex.Store<RootState>(store)

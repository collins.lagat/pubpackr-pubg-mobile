import * as firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import { firebaseConfig } from './config'

const project = firebase.initializeApp(firebaseConfig)

project.firestore().settings({
    cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED,
})

project
    .firestore()
    .enablePersistence()
    .catch(err => {
        if (err.code == 'failed-precondition') {
            // Multiple tabs open, persistence can only be enabled
            // in one tab at a a time.
            // ...
            console.log('Precondition Failed: Multiple Tabs Open')
        } else if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
            // ...
            console.log('Unimlemented')
        }
    })

export const db = project.firestore()

export const storage = project.storage()
